// app/routes.js
module.exports = function(app, passport) {
  app.get('/', function(req, res){

    res.render('index', { title: 'Express', user: req.user });
  });

  // // =====================================
  // // HOME PAGE (with login links) ========
  // // =====================================
  // app.get('/', function(req, res) {
  //   res.render('index.ejs'); // load the index.ejs file
  // });

  // =====================================
  // LOGIN ===============================
  // =====================================
  // show the login form
  app.get('/login', function(req, res) {

    // render the page and pass in any flash data if it exists
    res.render('login', { message: req.flash('loginMessage'), user: req.user }); 
  });

  // process the login form
  // app.post('/login', do all our passport stuff here);

  // =====================================
  // SIGNUP ==============================
  // =====================================
  // show the signup form
  app.get('/signup', function(req, res) {
    // render the page and pass in any flash data if it exists
    if (req.isAuthenticated())
      res.redirect('/profile');
    else 
      res.render('signup', { message: req.flash('signupMessage'), user: req.user });
  });

  // // process the signup form
  // // app.post('/signup', do all our passport stuff here);

  // =====================================
  // PROFILE SECTION =====================
  // =====================================
  // we will want this protected so you have to be logged in to visit
  // we will use route middleware to verify this (the isLoggedIn function)
  app.get('/profile', isLoggedIn, function(req, res) {

    res.render('profile.jade', {
      user : req.user // get the user out of session and pass to template
    });
  });

  // =====================================
  // LOGOUT ==============================
  // =====================================
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

  

  // process the signup form
  app.post('/signup', passport.authenticate('local-signup', {
    successRedirect : '/profile', // redirect to the secure profile section
    failureRedirect : '/signup', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));

  // process the login form
  app.post('/login', passport.authenticate('local-login', {
    successRedirect : '/profile', // redirect to the secure profile section
    failureRedirect : '/login', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  }));

    /**
  *
  * Update Employee
  **/
 //  function updateEmployee(_id, name, address, callback) {
 //    employees.findById(_id, function (err, item) {
 //      if (err){
 //        callback(err, null);
 //      }
 //      else {
 //        item.updated = new Date();
 //        item.name = name;
 //        item.address = address;
 //        item.save(function (err, result) {
 //          callback(err, result);
 //        });
 //      }
 //    });
 //  }
 // app.get('/updateEmployee', routes.updateEmployee);
 

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
  // if user is authenticated in the session, carry on 
  if (req.isAuthenticated())
    return next();

  // if they aren't redirect them to the home page
  res.redirect('/');
}
