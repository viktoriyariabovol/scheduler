'use strict';

/**
 * Generic require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
    if (!req.isAuthenticated()) {
        // return res.send(401, 'User is not authorized');
        return res.render('401.jade', {
            // user : req.user // get the user out of session and pass to template
        });
    }
    next();
};