/*global module:false*/
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'public/javascripts/controllers/*.js', 'public/javascripts/services/*.js', 'routes/*.js', 'config/*.js' ],
      options: {

        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },

    // concat: {  
    //   app: { 
    //     options: {
    //       separator: ';'
    //     },
    //     src: [
    //     'public/javascripts/controllers/*.js',
    //     'public/javascripts/services/*.js'
    //     ],
    //     dest: 'public/javascripts/app.js'
    //   }
    // },  

    nodemon: {
      dev: {
        options: {
          file: 'app.js',
          args: [],
          ignoredFiles: ['public/**'],
          watchedExtensions: ['js'],
          nodeArgs: ['--debug'],
          delayTime: 1,
          env: {
            PORT: 3000
          },
          cwd: __dirname
        }
      }
    },

    watch: {
     jade: {
      files: ['app/views/**'],
      options: {
        livereload: true,
      },
    },
    js: {
      files: ['gruntfile.js', 'app.js', 'app/**/*.js', 'public/javascripts/controllers/*.js', 'routes/*.js', 'public/javascripts/services/*.js', 'config/*.js'],
      tasks: ['jshint'],
      options: {
        livereload: true,
      },
    },
    css: {
      files: ['public/stylesheets/**'],
      options: {
        livereload: true
      }
    }   

  },
  mochaTest: {
    options: {
      reporter: 'spec',
      require: 'app.js'
    },
    src: ['tests/mocha/**/*.js']
  },
  karma: {
    unit: {
      configFile: 'tests/karma/karma.conf.js'
    }
  }
});

  grunt.loadNpmTasks('grunt-contrib-jshint');
  // grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-mocha-test');


  // Default task.
  grunt.registerTask('default', ['jshint']);
  grunt.registerTask('test', ['mochaTest', 'karma:unit']);

};
