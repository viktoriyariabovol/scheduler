define(['./module'], function (directives) {
  'use strict';

  directives.directive('myUser', function(User){
    return {
      restrict: 'E',
      scope: {
        user: '='
      },
      template: '',
      link: function(scope){
        User.setUser(scope.user);      
      }
    }
  });
});