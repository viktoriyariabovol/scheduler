define(['./module'], function (directives) {
  'use strict';

  directives.directive('activityTable', function(TimeTracking, $timeout, $http, CurrentDate){
    return {
      restrict: 'E',
      scope: {
        yearMonth: '=',
        update: '&'
      },
      template: 
      '<div ng-controller="ModalDemoCtrl">'+
      '  <script type="text/ng-template" id="myModalContent">' +
      '      <div class="modal-header">' +
      '          <h3>Edit</h3>' +
      '      </div>' +
      '      <div class="modal-body" ng-controller="TimepickerDemoCtrl">' +
      '       <div class="row">' +
      '          <div class="col-sm-1">' +
      '          </div>' +


      '          <div class="col-sm-2">' +
      '            <p>Checkin time</p>' +
      '            <div ng-model="item.checkinTime" ng-change="countTotal()" style="display: inline-block;" class="timepicker">' +
      '              <timepicker hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>' +
      //'              <pre>Time is: {{item.checkinTime | date:\'shortTime\'}}</pre>' +
      '            </div>' +
      '          </div>' +
      '          <div class="col-sm-2">' +
      '            <p>Checkout time</p>' +
      '            <div ng-model="item.checkoutTime" ng-change="countTotal()" style="display: inline-block;" class="timepicker">' +
      '              <timepicker hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>' +
      //'              <pre>Time is: {{item.checkoutTime | date:\'shortTime\' }}</pre>' +
      '            </div>' +
      '          </div>' +
      '          <div class="col-sm-2">' +
      '            <p>Exclude time</p>' +
      '            <div ng-model="item.excludeTime" ng-change="countTotal()" style="display: inline-block;" class="timepicker">' +
      '              <timepicker hour-step="hstep" minute-step="mstep" show-meridian="ismeridian"></timepicker>' +
      //'              <pre>Time is: {{item.excludeTime | date:\'shortTime\' }}</pre>' +
      '            </div>' +
      '          </div>' +
      '          <div class="col-sm-4">' +
      '            <p>Total</p>' +
      '            <div ng-model="timeTotal1" style="display: inline-block;">' +
      '              <pre>Time: {{timeTotal1 | date:\'shortTime\'}}</pre>' +
      '            </div>' +
      '          </div>' +
      '         </div>' +
      '          <div class="modal-footer">' +
      '               <button class="btn btn-primary" ng-disabled="disabled1" ng-click="ok()">OK</button>' +
      '               <button class="btn btn-warning" ng-click="cancel()">Cancel</button>' +
      '          </div>' +
      '      </div>' +
      '  </script>' +
      '  <table class="table table-striped">' +
      '  <thead>' +
      '    <tr>' +
      '      <th>CheckIn time</th>' +
      '      <th>CheckOut time</th>' +
      '      <th>Excluded hours</th>' +
      '      <th>Total time</th>' +
      '      <th></th>' +
      '      <th></th>' +
      '    </tr>' +
      '  </thead>' +
      '  <tbody>' +
      '    <tr ng-repeat="item in list | orderBy:\'checkinTime\'  ">' +
      '      <td>{{item.checkinTime | date:"yyyy-MM-dd HH:mm"}}</td>' +
      '      <td>{{item.checkoutTime | date:"yyyy-MM-dd HH:mm"}}</td>' +
      '      <td>{{item.excludeTime | date:"HH:mm" }}</td>' +
      '      <td>{{item.timeTotal}}</td>' +
      '      <td><button type="submit" class="btn btn-default"  ng-click="open(item)">Edit</button></td>' + // edit(item)
      '      <td><button type="submit" class="btn btn-danger" ng-click="delete(item._id)">Delete</button></td>'+
      '    </tr>' +
      '    </tbody>' +
      '  </table>' +
      '</div>',
      link: function(scope){

        scope.$on('updateView', function(event) {
          scope.update();         
        });


        scope.$watchCollection('yearMonth', function(){          
          scope.update();
        }); 

        scope.update = function(){
          $http({method: 'GET', url: '/api/tt/' + scope.yearMonth.year + '/' + scope.yearMonth.month}).
          success(function(data, status, headers, config) {
            scope.list = data;
          }).
          error(function(data, status, headers, config) {
          });
        };

        scope.delete = function(id){
          $http.delete('/api/tt/'+id, {
          }).
          success(function(data, status, headers, config) {
              // this callback will be called asynchronously
              // when the response is available
              CurrentDate.updateView();
            }).
          error(function(data, status, headers, config) {
            console.log(data);
              // called asynchronously if an error occurs
              // or server returns response with an error status.
            });
        };
      }
    }
  });

});