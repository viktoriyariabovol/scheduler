  // Based on https://github.com/Mischi/angularjs-imageupload-directive
  //
define(['./module', 
        'load-image',
        'load-image-orientation',
        'load-image-exif',
        'load-image-exif-map',
        'load-image-meta',
        'jquery',
        'jquery-jcrop'
  ], function (directives, loadImage, Jcrop) {
//define(['./module'], function (directives) {  
  'use strict';

  directives.directive('imageUpload', function ($q) {
    'use strict';
    /*jshint loopfunc: true*/
    /*jshint laxcomma: true*/

    var URL = window.URL || window.webkitURL;

    return {
      restrict: 'E',
      scope: {
        imageupload: '=',
        resizeMaxHeight: '@?',
        resizeMaxWidth: '@?',
        resizeQuality: '@?',
        resizeType: '@?',
        none: '=imageUpload',
        currentValue: '=current'
      },
      replace: true,
      template:
        '<div>' +
        //'<img ng-src="{{currentValue}}" class="profile-photo" />' +
        //'<label for="{{inputName}}">Select upload photo: </label>' +
        '<h2>Select an image file</h2>' +
        '<p>Or <strong>drag &amp; drop</strong> an image file onto this webpage.</p>' +
        '<br>' +
        '<p><input id="file-input" name="file-input" type="file" accept="image/*" /></p>' +
        '<br>' +
        '<h2>Result</h2>' +
        '<div id="result" style="display: none;" class="result">' +
        '  <p>This demo works only in browsers with support for the <a href="https://developer.mozilla.org/en/DOM/window.URL">URL</a> or <a href="https://developer.mozilla.org/en/DOM/FileReader">FileReader</a> API.</p>' +
        '</div>' +
        '<br>' +
        '<p id="actions" style="display:none;">' +
        '  <button class="btn btn-default btn-sm" type="button" id="edit">Edit</button>' +
        '  <button class="btn btn-default btn-sm" type="button" id="crop">Crop</button>' +
        '</p>' +
        '<input type="hidden" name="test" value="{{currentValue}}" />' +
        '</div>', 
        //'<a class="replace-file-input" ng-click="openFileDialog()">{{inputLabel}}</a>',

      link: function postLink(scope, element, attrs, ctrl) {

        scope.inputName = attrs.name;
        scope.inputLabel = attrs.label;

        scope.applyScope = function (dataURL) {
          scope.$apply(function () {
            scope.currentValue = dataURL;

            console.log(scope.currentValue);

            //scope.inputLabel = file.name; // update label with filename
          });
        };

      var result = $('#result'),
        thumbNode = $('#thumbnail'),
        actionsNode = $('#actions'),
        currentFile,
        editImg = function(){
          var imgNode = result.find('img, canvas'),
              img = imgNode[0];

          imgNode.Jcrop({
              setSelect: [40, 40, img.width - 40, img.height - 40],
              onSelect: function (coords) {
                  coordinates = coords;
              },
              onRelease: function () {
                  coordinates = null;
              }
          }).parent().on('click', function (event) {
              event.preventDefault();
          });
        },
        replaceResults = function (img) {
            var content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>');
            } else {
                content = $('<a target="_blank">').append(img)
                    .attr('download', currentFile.name)
                    .attr('href', img.src || img.toDataURL());
                scope.applyScope(img.toDataURL());
            }
            result.children().replaceWith(content);
            result.show();
            if (img.getContext) {
                actionsNode.show();
            }

            if(scope.editMode) {
              editImg();
              scope.editMode = false;
            }            
        },
        displayImage = function (file, options) {
            currentFile = file;
            if (!loadImage(
                    file,
                    replaceResults,
                    options
                )) {
                result.children().replaceWith(
                    $('<span>Your browser does not support the URL or FileReader API.</span>')
                );
            }
        },   
        dropChangeHandler = function (event) {
            // event.preventDefault();
            // event = event.originalEvent;
            var target = event.dataTransfer || event.target,
                file = target && target.files && target.files[0],
                options = {
                    maxWidth: scope.resizeMaxWidth,
                    maxHeight: scope.resizeMaxHeight,
                    canvas: true
                };
            if (!file) {
                return;
            }
            scope.file = file;
            thumbNode.hide();
            loadImage.parseMetaData(file, function (data) {
                if (data.exif) {
                    options.orientation = data.exif.get('Orientation');
                }
                displayImage(file, options);
            });
        },
        editHandler = function () {
            var options = {
                    maxWidth: 600, // ToDo: use options.
                    maxHeight: 600, // ToDo: use options.
                    canvas: true
                };
            //scope.file;
            // thumbNode.hide();
            loadImage.parseMetaData(scope.file, function (data) {
                if (data.exif) {
                    options.orientation = data.exif.get('Orientation');
                }
                displayImage(scope.file, options);
            });
            
        },
        coordinates;


      // Hide URL/FileReader API requirement message in capable browsers:
      if (window.createObjectURL || window.URL || window.webkitURL || window.FileReader) {
          result.children().hide();
      }
      $(document)
        .on('dragover', function (evt) {
            evt.preventDefault();
            evt = evt.originalEvent;
            evt.dataTransfer.dropEffect = 'copy';
          })
        .on('drop', dropChangeHandler);

      $('#file-input').on('change', dropChangeHandler);

      $('#edit').on('click', function (event) {
        event.preventDefault();
        scope.editMode = true;
        editHandler();
      });

      $('#crop').on('click', function (event) {
        event.preventDefault();
        var img = result.find('img, canvas')[0];
          if (img && coordinates) {
            console.log(coordinates);
            console.log(result.width());
              replaceResults(loadImage.scale(img, {
                  left: coordinates.x,
                  top: coordinates.y,
                  sourceWidth: coordinates.w,
                  sourceHeight: coordinates.h,
                  maxWidth: scope.resizeMaxWidth
              }));
              coordinates = null;
          }
      });  
      }
    };
  });
});
