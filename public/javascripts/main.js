/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
require.config({

    paths: {
        'angular'   : '../lib/angular/angular',
        'angular-route': '../lib/angular-route/angular-route',
        'domReady'  : '../lib/requirejs-domready/domReady',
        'moment'    : '../lib/moment/moment',
        'jquery'    : '../lib/jquery/dist/jquery',
        'jquery-jcrop'    : '../lib/load-image/vendor/jquery.jcrop',

        'load-image'              : '../lib/load-image/load-image',
        'load-image-orientation'  : '../lib/load-image/load-image-orientation',
        'load-image-exif'         : '../lib/load-image/load-image-exif',
        'load-image-exif-map'     : '../lib/load-image/load-image-exif-map',
        'load-image-meta'         : '../lib/load-image/load-image-meta'

        //'bootstrap': 'ui-bootstrap-tpls-0.11.0-SNAPSHOT'
    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        },
        'jquery': {
            exports: 'jquery'
        },
        'jquery.Jcrop': {
            deps: ['jquery'],
            exports: 'jQuery.Jcrop'
        },
    },        

    deps: [
        // kick start application... see bootstrap.js
        './bootstrap'
    ]
});