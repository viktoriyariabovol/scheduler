define(['./module'], function (services) {
  'use strict';

  services.service('User', 
    function($rootScope) {

      this.setUser = function (user) {
        $rootScope.$broadcast('userUpdated', user);    
      };

    });

});
