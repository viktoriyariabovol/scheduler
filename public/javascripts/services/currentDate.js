define(['./module'], function (services, $scope) {
  'use strict';

  services.service('CurrentDate', 
    function($rootScope) {

      this.setDate = function (date) {
        $rootScope.$broadcast('dateChanged', date);    
      };

      this.updateView = function () {
        $rootScope.$broadcast('updateView');    
      };

    });

});


