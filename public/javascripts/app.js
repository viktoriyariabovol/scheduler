/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 */
define([
    'angular',
    'angular-route',
    'moment',
    'bootstrap',
    'jquery',
    // 'load-image',
    './controllers/index',
    './directives/index',
    //'./filters/index',
    './services/index'
], function (angular) {
    'use strict';

    return angular.module("app", [
      'app.controllers',
      'app.directives',
      'app.services',
      'ui.bootstrap',
      'ngRoute'
    ]);

});
