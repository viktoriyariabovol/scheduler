define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('DatepickerDemoCtrl', function ($scope, CurrentDate) {
    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

  //$scope.showWeeks = true;
  $scope.minMode='day';
  $scope.datepickerMode= 'day';
  $scope.toggleWeeks = function () {
    $scope.showWeeks = ! $scope.showWeeks;
  };

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = ( $scope.minDate ) ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.dateOptions = {
    'year-format': "'yy'",
    'starting-day': 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
  $scope.format = $scope.formats[0];


  $scope.myMove = function( direction ) {
    var mDt = moment($scope.dt);
    var newDate;
    if (direction == 1) {
      newDate = mDt.add('days',-7);
    } 
    else if (direction == 2) {
     newDate = mDt.add('days', 7);
    }
    else {
      console.log('wrong');
    } 
    $scope.dt = newDate;

    CurrentDate.setDate($scope.dt);
  };

  $scope.$watch('dt', function(){
    CurrentDate.setDate($scope.dt);
  });

});
});