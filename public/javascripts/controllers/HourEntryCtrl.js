define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('hourEntryCtrl', function ($scope, $q) {
    var deferred = $q.defer();
    $scope.$on('dateChanged', function(event, date) {
      $scope.selectedDate = new Date(date);
      
      $scope.currentYearMonth = {
        year: moment($scope.selectedDate).year(), 
        month: moment($scope.selectedDate).month()
      };
    });

    setTimeout(function() {
      $scope.$apply(function() {
       // console.log("$scope.selectedDate");
       // console.log($scope.currentYearMonth);
     });
    },300);

    

 });
    });