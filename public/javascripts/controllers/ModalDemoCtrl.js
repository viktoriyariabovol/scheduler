define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('ModalDemoCtrl', ['$scope', '$modal', '$log', '$http','CurrentDate', function($scope, $modal, $log, $http, CurrentDate) {

    $scope.open = function (item) {

      console.log(item);

      $scope.item = item;


      var modalInstance = $modal.open({
        templateUrl: 'myModalContent',
        //controller: controllers.ModalInstanceCtrl,
        scope: $scope,
        resolve: {
          item: function () {
            return item;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });

      $scope.ok = function () {
        $scope.updateTime();
        modalInstance.close();
      };

      $scope.cancel = function () {
        modalInstance.dismiss('cancel');
      };

      $scope.updateTime = function () {
        console.log($scope.item);

        $http.put('/api/tt/'+$scope.item._id, {
          "checkinTime": $scope.item.checkinTime,
          "checkoutTime": $scope.item.checkoutTime,
          "excludeTime": $scope.item.excludeTime,
          "Total": $scope.timeTotal1
          // "date.year": $scope.currentYearMonth.year,
          // "date.month": $scope.currentYearMonth.month
        }).
        success(function(data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
            CurrentDate.updateView();
         }).
        error(function(data, status, headers, config) {
          console.log(data);
            // called asynchronously if an error occurs
            // or server returns response with an error status.
         });

       };

      
        var exclude = moment.utc(item.checkinTime);
        $scope.excludeTime = new Date(moment({months: exclude.months(), days: exclude.date(), hour: 1, minute: 0}));

      $scope.$watchCollection('[item.checkinTime, item.checkoutTime, item.excludeTime]', function() {
         $scope.countTotal1();  
      });

      $scope.countTotal1 = function() {
        //converting utc to local time and setting exclude time
        $scope.checkinTime = moment.utc(item.checkinTime).local();
        $scope.checkoutTime = moment.utc(item.checkoutTime).local();
        $scope.excludeTime = moment.utc(item.excludeTime).local();

        $scope.ms = moment($scope.checkoutTime).diff(moment($scope.checkinTime));
        $scope.d = moment.duration($scope.ms);
        $scope.diff = Math.floor($scope.d.asHours()) + moment.utc($scope.ms).format(":mm:ss");
        console.log($scope.checkoutTime.diff($scope.checkinTime));
        $scope.mcOut = moment($scope.checkoutTime);
        $scope.mcIn = moment($scope.checkinTime);
        $scope.mcEx = moment($scope.excludeTime);
        var cout = moment.duration($scope.checkoutTime).asHours();
        var cin = moment.duration($scope.checkinTime).asHours();
        var cexcl = moment.duration($scope.excludeTime).asHours();

         if ((cout-cin-cexcl)<0){
          $scope.timeTotal1 = "it must be positive value";
          $scope.disabled1 = true;
         }
         else {
          $scope.timeTotal1 = $scope.mcOut.subtract($scope.checkinTime).subtract($scope.excludeTime).format('HH:mm');
          $scope.disabled1 = false;
         }
      };
      
    };
  }]);
});
