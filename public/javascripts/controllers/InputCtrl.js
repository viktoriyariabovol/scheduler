define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('InputCtrl', function ($scope) {
    $scope.EMAIL_REGEXP = /^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*$/i;

    //$scope.birth = new Date();
    $scope.submitForm = function(event) {

      $scope.submitted = true;

      if ($scope.userForm.$valid) {
        console.log('form is valid');
        return true;
      }
      else {
        event.preventDefault();
        console.log('invalid');
      }
      
    };

  });
});