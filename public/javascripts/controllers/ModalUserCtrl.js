define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('ModalUserCtrl', ['$scope', '$modal','$log', function($scope, $modal, $log) {

    $scope.open = function (item) {
      console.log(item);
      $scope.item = item;

      var modalInstance = $modal.open({
        templateUrl: 'ModalContent',
        scope: $scope,
        resolve: {
          item: function () {
            return item;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });

      $scope.ok = function () {
        modalInstance.close();
        console.log("let's change");
        console.log($scope.item.email);
      };

      $scope.cancel = function () {
        modalInstance.dismiss('cancel');
      };

      
    };
  }]);
});
