define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('HeaderCtrl', function ($scope) {

    $scope.$on('userUpdated', function(event, user) {
      $scope.user = user;
    });

    //datepicker 
    $scope.date = new Date();

    $scope.toggleMin = function() {
      $scope.minDate = ( $scope.minDate ) ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };



  });
});