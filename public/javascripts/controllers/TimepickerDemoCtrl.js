define(['./module'], function (controllers) {
  'use strict';

  controllers.controller('TimepickerDemoCtrl', function ($scope, $http, $window, $timeout, CurrentDate) {

    $scope.mytimeIn = new Date();
    $scope.mytimeOut = new Date();
    $scope.mytimeExclude = new Date();
    $scope.excludeTime = new Date(moment({hour: 1, minute: 0}));

    $scope.hstep = 1;
    $scope.mstep = 15;

    $scope.options = {
      hstep: [1, 2, 3],
      mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.ismeridian = false;
    $scope.toggleMode = function() {
      $scope.ismeridian = ! $scope.ismeridian;
    };

    $scope.$on('dateChanged', function(event, date) {
      $scope.mytimeIn = $scope.mytimeOut = new Date(date);
    });


    $scope.changed = function () {
      console.log('Time changed to: ' + $scope.mytime);
    };
    $scope.updateHours = function () {
      console.log('update hours');
    };

    $scope.countTotal = function() {

    //CheckOut time- CheckIn time
    $scope.ms = moment($scope.mytimeOut).diff(moment($scope.mytimeIn));
    $scope.d = moment.duration($scope.ms);
    $scope.diff = Math.floor($scope.d.asHours()) + moment.utc($scope.ms).format(":mm:ss");


    $scope.mcOut = moment($scope.mytimeOut);
    $scope.mcIn = moment($scope.mytimeIn);
    $scope.mcEx = moment($scope.excludeTime);
    var a = moment.duration($scope.mytimeOut).asHours();
    var b = moment.duration($scope.mytimeIn).asHours();
    var c = moment.duration($scope.excludeTime).asHours();

    if ((a-b-c)<0){
      $scope.timeTotal = "must be positive value"
      $scope.disabled = true;
    }
    else {
      $scope.timeTotal = $scope.mcOut.subtract($scope.mytimeIn).subtract($scope.excludeTime).format('HH:mm');
      $scope.disabled = false;
    }
  };

  $scope.$watchCollection('[mytimeIn, mytimeOut, excludeTime]', function() {
    $scope.countTotal();  
  });



  $scope.submitTime = function () {
    if ($scope.timeTotal.length > 5) {
      return 
    }
    else {
      console.log('submit time');
      var month = $scope.currentYearMonth.month;
      var year  = $scope.currentYearMonth.year
      ;
      $http.post('/api/tt/'+year+'/'+month, {
        "checkinTime": $scope.mytimeIn,
        "checkoutTime": $scope.mytimeOut,
        "excludeTime": $scope.excludeTime,
        "timeTotal": $scope.timeTotal,
        "date.year": $scope.currentYearMonth.year,
        "date.month": $scope.currentYearMonth.month
      }).
      success(function(data, status, headers, config) {
        // this callback will be called asynchronously
        // when the response is available
        CurrentDate.updateView();
      }).
      error(function(data, status, headers, config) {
        console.log(data);
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    };
  }
});
});
