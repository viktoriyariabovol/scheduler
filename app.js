'use strict';

var authorization = require('./routes/middlewares/authorization');

/**
 * Module dependencies.
 */
 var fs = require('fs');
 var express = require('express');
 var http = require('http');
 var path = require('path');
 var moment = require('moment');
 var mongoose = require('mongoose');
 var passport = require('passport');
 var flash    = require('connect-flash');
 
 mongoose.connect('mongodb://localhost:27017/nodetest1');

 var db = mongoose.connection;
 db.on('error', console.error.bind(console, 'connection error:'));
 db.once('open', function callback () {
 });

require('./config/passport')(passport, fs); // pass passport for configuration

//require('./config/passport')(passport); // pass passport for configuration

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
//app.use(express.json());
//app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
//fix 
//app.use(express.bodyParser());
app.use(express.json());
app.use(express.urlencoded({uploadDir:'./uploads'}));

// required for passport
app.use(express.session({ secret: 'victorias secret' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//ROUTES
// var routes = require('./routes');
// app.get('/monthlist', routes.monthlist(db));
// app.get('/userlist', routes.userlist(db));
// routes ======================================================================
require('./routes/pass.js')(app, passport); // load our routes and pass in our app and fully configured passport


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));

});



//SHEMA
var Schema = mongoose.Schema;

var Timetracking = new Schema ({
  personId: {type: String, required: true},
  checkinTime: {type: Date, required: true },
  checkoutTime: {type: Date, required: true },
  excludeTime: {type: Date, required: true },
  timeTotal: {type: String},
  date: {
    year: {type: String},
    month: {type: String}
  }
});


var TimeModel = mongoose.model('Time', Timetracking, 'timetracking');  


//TimeTracking API start
// helper functions
var checkMonth = function(year, month) {
  var start = new Date(year, month),
  nextMonth = month > 11 ? 1 : ++month,
  end = new Date(year, nextMonth);

  return {"$gte": start, "$lt": end};
};

//CRUD
app.get('/api/tt/:year/:month', function (req, res){

  return TimeModel.find({personId: req.user._id, checkinTime: checkMonth(req.params.year, req.params.month)}, function (err, times) {
    if (!err) {
      return res.send(times);  
    } else {
      return console.log(err);
    }
  });
});

app.get('/api/tt/:id', function (req, res){
  return TimeModel.findById(req.params.id, function (err, time) {
    if (!err) {
      return res.send(time);
    } else {
      return console.log(err);
    }
  });
});

app.post('/api/tt/:year/:month', function (req, res){
  var time;
  console.log("POST: ");
  console.log(req.body);

  time = new TimeModel({
    // TODO: personId: Session.getUser.getId, //req.body.personId,
    personId: req.user._id,
    checkinTime: req.body.checkinTime,
    checkoutTime: req.body.checkoutTime,
    excludeTime: req.body.excludeTime,
    timeTotal: req.body.timeTotal,
    date: {
      year: req.params.year,
      month: req.params.month
    }
  });

  time.save(function (err) {
    if (!err) {
      return console.log("created");
    } else {
      return console.log(err);
    }
  });
  return res.send(time);
});

app.put('/api/tt/:id', function (req, res){
  return TimeModel.findById(req.params.id, function (err, time) {

    console.log(time); 
    console.log("req.body");
    console.log(req.body);

    time.checkinTime = req.body.checkinTime;
    time.checkoutTime = req.body.checkoutTime;
    time.excludeTime = req.body.excludeTime;
    time.timeTotal = req.body.Total;


    return time.save(function (err) {
      if (!err) {
        console.log("updated");
      } else {
        console.log(err);
      }
      return res.send(time);
    });

  });
});


app.delete('/api/tt/:id', function (req, res) {
  var entry = TimeModel.findById(req.params.id);
  entry.remove(function (err) {
    if (!err) {
      console.log("removed");
      return res.send('');
    } else {
      console.log(err);
    }
  });
});


////////////////////////// TimeT end

app.get('/hour-entry', authorization.requiresLogin, function (req, res){
  res.render('hour-entry', { user: req.user }); 
});